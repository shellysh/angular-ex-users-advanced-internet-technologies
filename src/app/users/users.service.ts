import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class UsersService {
  http:Http;
  getUsers(){
       return this.http.get('http://localhost/ex/users');
  }

  //constructor() { }

  constructor(http:Http){ 
     this.http = http;
  }

}
